from flask import Blueprint, render_template, send_file, jsonify, request
import database
import qq 
import json
import maya
from .models import DBSession

api = Blueprint('api', __name__)

def get_leaderboard():
    all_users = qq.get_all_users()
    user_data = [qq.get_user_data(name) for name in all_users]
    sorted_user_data = sorted(user_data, key=lambda k: k['percentage']) 
    formatted_user_data = [(get_row_color(d['percentage']), d['username'], d['percentage'], "🔌 " if d['charging'] else  "🔋 ", maya.parse(d['updated_at'], timezone='US/Eastern')) for d in sorted_user_data]
    return [(d[0], d[1], d[2], d[3], d[4].epoch) for d in formatted_user_data if (maya.now() - d[4]).total_seconds() < 60*10]

@api.route("/driest")
def driest():
    all_users = get_leaderboard()
    human_text = "no one"
    if len(all_users) >= 1:
        lowest = all_users[0]
        human_text = "%s at %d percent" % (lowest[1], lowest[2])

    return jsonify({
      "fulfillmentText": human_text,
      "payload": {
        "google": {
          "expectUserResponse": False,
          "richResponse": {
          "items": [{
              "simpleResponse":{
                "textToSpeech": human_text
              }
            }
          ]
        }
      }
    }})

@api.route("/stats")
def stats():
  return json.dumps(get_leaderboard())
