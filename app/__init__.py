from flask import Flask

from .base_controller import base
from .api_controller import api
from .models import DBSession
import os

def create_app():
  app = Flask(__name__, instance_relative_config=True)

  app.config['SECRET_KEY'] = os.environ.get('FLASK_SECRET') or 'supersecret'

  if os.environ.get('PRODUCTION') and not os.environ.get('FLASK_SECRET'):
    raise RuntimeError("Production mode requires FLASK_SECRET environment variable to be exported!")

  app.register_blueprint(base)
  app.register_blueprint(api, url_prefix='/api')

  return app
