from flask import Blueprint, send_from_directory
import os

base = Blueprint('base', __name__)

@base.route("/update", methods=['GET', 'POST'])
def update():
  try:
    os.system("git pull origin master")
    os.system(f"kill -HUP {os.getpid()}")
    return "Done", 200
  except Exception as e:
    print(e)
    return "An unexpected error has occurred, please try again later", 500
