An API for the Homebrew App Store to record download statistics and feedback information.

## Usage
There are four containers involved with this project:

- **api** to actually receive events and serve stats
- **redis** for recording incoming events
- **postgres** for longterm storage of all events
- **processor** to write those events from redis into longterm storage in postgres at some interval

```
docker-compose build
docker-compose up
```
