from app.models import Update, DBSession

def batch_insert_update(updates):
  with DBSession() as session:
    session.bulk_save_objects(list(map(lambda update: Update(**update), updates)))
    session.commit()
